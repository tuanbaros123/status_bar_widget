library status_bar_widget;

import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

class StatusBarWidget extends StatefulWidget {
  final Widget child;
  final bool whiteStatusBar;
  final Color statusBarColor;
  const StatusBarWidget({required Key key, required this.child, this.whiteStatusBar = false, this.statusBarColor = Colors.transparent}) : super(key: key);

  static List<bool> stackData = [];
  static List<Color> stackColorData = [];

  static changeWhiteStatusBar(bool whiteStatusBar) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(whiteStatusBar);
    stackData.last = whiteStatusBar;
  }

  static changeStatusBarColor(Color color) {
    FlutterStatusbarcolor.setStatusBarColor(color);
    stackColorData.last = color;
  }

  @override
  _LifecycleWatcherState createState() => _LifecycleWatcherState();
}

class _LifecycleWatcherState extends State<StatusBarWidget> with WidgetsBindingObserver {

  static List<bool> stackData = [];
  static List<Color> stackColorData = [];

  @override
  void initState() {
    super.initState();
    stackData = StatusBarWidget.stackData;
    stackColorData = StatusBarWidget.stackColorData;
    FlutterStatusbarcolor.setStatusBarColor(widget.statusBarColor);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(widget.whiteStatusBar);
    stackData.add(widget.whiteStatusBar);
    stackColorData.add(widget.statusBarColor);
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    debugPrint("dispose");
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void deactivate() {
    debugPrint("deactivate");
    try {
      stackData.removeLast();
      stackColorData.removeLast();
      FlutterStatusbarcolor.setStatusBarColor(stackColorData.last);
      FlutterStatusbarcolor.setStatusBarWhiteForeground(stackData.last);
    } catch (e) {
      debugPrint("$e");
    }
    super.deactivate();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    debugPrint("didChangeAppLifecycleState");
    await FlutterStatusbarcolor.setStatusBarColor(stackColorData.last);
    await FlutterStatusbarcolor.setStatusBarWhiteForeground(stackData.last);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
